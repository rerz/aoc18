use itertools::Itertools;
use itertools::structs::Group;
use itertools::zip;

const INPUT: &str = include_str!("../res/day2.input.txt");

pub fn solve_part_one() {
    let mut count_two = 0;
    let mut count_three = 0;

    for line in INPUT.lines() {
        let chars = line.chars().sorted();
        let groups = chars.iter().group_by(|c| *c);

        groups.into_iter()
            .map(|(key, group)| group.count())
            .filter(|count| *count == 2 || *count == 3)
            .unique()
            .for_each(|num| {
                if num == 2 {
                    count_two += 1;
                }

                if num == 3 {
                    count_three += 1;
                }
            });
    }


    println!("{}", count_two * count_three);
}

pub fn solve_part_two() {

    for (a, b) in INPUT.lines().tuple_combinations() {
        let a: &str = a.trim();
        let b: &str = b.trim();

        let a_nums = a.chars().map(|c| c as i32).collect::<Vec<_>>();
        let b_nums = b.chars().map(|c| c as i32).collect::<Vec<_>>();

        let diffs: Vec<_> = zip(a_nums, b_nums)
            .enumerate()
            .map(|(i, (a_num, b_num))| (i, a_num - b_num))
            .filter(|(_, num)| *num != 0)
            .collect::<Vec<_>>();

        if diffs.len() == 1 {
            let mut chars = a.chars().collect::<Vec<_>>();
            chars.remove(diffs[0].0);
            let solution = chars.iter().collect::<String>();

            println!("{}", solution);
            break;
        }
    }
}