extern crate itertools;
extern crate regex;
extern crate failure;

pub type Result<T> = ::std::result::Result<T, failure::Error>;

mod day1;
mod day2;
mod day3;

fn main() -> Result<()> {
    day3::solve_part_two()?;

    Ok(())
}
