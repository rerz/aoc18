use std::collections::HashMap;
use itertools::zip;
use itertools::Itertools;

use regex::Regex;

use super::Result;

const INPUT: &str = include_str!("../res/day3.input.txt");

#[derive(Default, Debug)]
pub struct Rectangle {
    pub id: u32,
    pub x: u32,
    pub y: u32,
    pub w: u32,
    pub h: u32,
}

pub fn parse_rectangles() -> Result<Vec<Rectangle>> {
    let mut rectangles = Vec::<Rectangle>::new();

    let re = Regex::new(r"^#(?P<id>[0-9]+) @ (?P<x>[0-9]+),(?P<y>[0-9]+): (?P<w>[0-9]+)x(?P<h>[0-9]+)?").unwrap();

    for line in INPUT.lines() {
        let captures = re.captures(line).unwrap();

        let mut rectangle = Rectangle::default();

        rectangle.id = captures.name("id").unwrap().as_str().parse()?;
        rectangle.x = captures.name("x").unwrap().as_str().parse()?;
        rectangle.y = captures.name("y").unwrap().as_str().parse()?;
        rectangle.w = captures.name("w").unwrap().as_str().parse()?;
        rectangle.h = captures.name("h").unwrap().as_str().parse()?;

        rectangles.push(rectangle);
    }
    Ok(rectangles)
}

pub fn build_map(rectangles: &[Rectangle]) -> HashMap<(u32, u32), u32> {
    let mut map = HashMap::<(u32, u32), u32>::new();

    for rectangle in rectangles {
        for x in rectangle.x..rectangle.x + rectangle.w {
            for y in rectangle.y..rectangle.y + rectangle.h {
                *map.entry((x, y)).or_insert(0) += 1;
            }
        }
    }

    map
}

pub fn solve_part_one() -> Result<()> {

    let rectangles = parse_rectangles()?;
    let map = build_map(&rectangles);

    println!("{}", map.values().filter(|value| **value > 1).count());

    Ok(())
}

pub fn solve_part_two() -> Result<()> {

    let rectangles = parse_rectangles()?;
    let map = build_map(&rectangles);

    for rectangle in rectangles {

        let mut coords = Vec::<(u32, u32)>::new();

        for x in rectangle.x .. rectangle.x + rectangle.w {
            for y in rectangle.y .. rectangle.y + rectangle.h {
                coords.push((x, y));
            }
        }

        if coords.iter().all(|coord| *map.get(coord).unwrap() == 1) {
            println!("{}", rectangle.id);
            break;
        }
    }

    Ok(())
}