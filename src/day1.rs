use std::collections::HashSet;

const INPUT: &str = include_str!("../res/day1.input.txt");

pub fn solve_part_one() {
    let solution: i32 = INPUT
        .lines()
        .map(|l| l.parse::<i32>().unwrap())
        .sum();

    println!("{}", solution);
}

pub fn solve_part_two() {
    let mut set = HashSet::<i32>::new();

    let cycle = INPUT
        .lines()
        .map(|l| l.parse::<i32>().unwrap())
        .cycle();

    let mut current = 0;
    let mut solution: i32 = 0;

    for num in cycle {
        if !set.insert(current + num) {
            solution = current + num;
            break;
        }

        current += num;
    }

    println!("{}", solution);
}